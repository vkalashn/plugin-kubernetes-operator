# Kubernetes Operator

The operator observes changes and triggeres the correct action within a namespace.

## Plugin Synchronization

If a micro service is deployed by using 

```
xfsc.kubernetes.io/component: xfsc.pcm.plugin
```

as label, the operator will pick it up and insert into the configured kong api and service/route to the api by using the metadata konfigured in the deployment annotations which are defined as:

```
xfsc.kubernetes.io/configuration: | 
              {"version":"v1","route":"/myFancyRoute", "name":"New Plugin","serviceguid":"19bafa36-8415-44e9-a2de-32852fefa6ef","routeguid":"30959362-e35b-4eb4-afe5-e8fdd6a3fecd"}
```
