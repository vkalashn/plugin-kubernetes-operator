package kubernetes

import (
	"github.com/spf13/viper"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var client *kubernetes.Clientset

func InitializeKubernetes() error {
	file := viper.GetString("KUBE_FILE")
	config, err := rest.InClusterConfig()
	if file != "" {
		config, err = clientcmd.BuildConfigFromFlags("", file)
	}

	if err != nil {
		return err
	}
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)

	if err != nil {
		return err
	}

	client = clientset

	return nil
}
