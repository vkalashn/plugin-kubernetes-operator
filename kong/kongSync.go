package kong

import (
	"strconv"
	"strings"

	"github.com/spf13/viper"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-kubernetes-operator/logger"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-kubernetes-operator/types"
	v1types "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/watch"
)

var log = logger.GetLogger()

func SyncKongServices(services *[]types.Metadata) {
	adminApi := viper.GetString("KONG_ADMIN_API")
	tag := "pcm-plugin"
	kr, err := kongListRoutes(adminApi, "", tag)

	if err != nil || kr == nil {
		log.Sugar().Error(err)
		return
	}

	for _, r := range kr {
		route := r.([]interface{})

		for _, ro := range route {
			data := ro.(map[string]interface{})

			routeId := data["id"].(string)
			serviceId := data["service"].(map[string]interface{})["id"].(string)
			if routeId != "" && serviceId != "" {
				var found = false
				for _, svc := range *services {
					if svc.Route == routeId || svc.ServiceGuid == serviceId {
						found = true
					}
				}

				if !found {
					err := kongDeleteRoute(routeId, adminApi)
					if err == nil {
						err = kongDeleteService(serviceId, adminApi)
					}
					if err != nil {
						log.Sugar().Errorln("Deletion not successfull.")
					}
				}
			}
		}
	}
}

func SyncKongService(event watch.EventType, svc *v1types.Service, metadata *types.Metadata) {
	adminApi := viper.GetString("KONG_ADMIN_API")
	str := []string{metadata.Name}
	name := strings.ReplaceAll(strings.Join(str, "-"), " ", "-")

	service, err := kongListService(adminApi, metadata.ServiceGuid)
	tags := []string{"pcm-plugin"}
	methods := []string{"GET", "PUT", "POST", "DELETE"}

	port := strconv.Itoa(int(svc.Spec.Ports[0].Port))
	protocol := string(svc.Spec.Ports[0].Protocol)
	host := svc.Name + "." + svc.Namespace + ".cluster.local"

	if err == nil {
		if len(service) > 0 {

			if event == watch.Deleted {
				err = kongDeleteRoute(metadata.RouteGuid, adminApi)
				if err == nil {
					err = kongDeleteService(metadata.ServiceGuid, adminApi)
				}
				if err != nil {
					log.Sugar().Errorln("Deletion not successfull.")
					return
				}
			}

			if event == watch.Modified || event == watch.Added {
				err = kongCreateService(metadata.ServiceGuid, name, protocol, host, "", port, adminApi, "PATCH", tags)
				if err == nil {
					err = kongCreateRoute(metadata.ServiceGuid, metadata.RouteGuid, name, metadata.Route, adminApi, "PATCH", tags, methods)
				}
				if err != nil {
					log.Sugar().Errorln("Deletion not successfull.")
					return
				}
			}

		} else {
			err := kongCreateService(metadata.ServiceGuid, name, protocol, host, "", port, adminApi, "POST", tags)

			if err == nil {
				err = kongCreateRoute(metadata.ServiceGuid, metadata.RouteGuid, name, metadata.Route, adminApi, "POST", tags, methods)
			}
			if err != nil {
				log.Sugar().Errorln("Deletion not successfull.")
				return
			}
		}
	}
}
